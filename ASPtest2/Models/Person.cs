﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace ASPtest2.Models
{
    public class Person
    {
        public Person(Name name )
        {
            this.name = name;
        }
        public string _id { get; set; }
        public int index { get; set; }
        public string balance { get; set; }
        public string picture { get; set; }
        public int age { get; set; }
        public string eyeColor { get; set; }
        public Name name { get; set; }
        public string company { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string address { get; set; }
        public string about { get; set; }
        public MyList<string> tags { get; set; }
        public MyList<Friend> friends { get; set; }
        public string favoriteFruit { get; set; }
        public static string[] Properties()
        {
            PropertyInfo[] properties = typeof(Person).GetProperties();
            string[] propNames = new string[properties.Length];
            for (int i = 0; i < properties.Length; i++)
            {
                propNames[i] = properties[i].Name;
            }
            return propNames;
        }
        public string[] ToArrayString()
        {
            
            PropertyInfo[] properties = typeof(Person).GetProperties();
            string[] propNames = new string[properties.Length];
            for (int i = 0; i < properties.Length; i++)
            {
                propNames[i] = properties[i].Name;
            }
            string[] res = new string[properties.Length];
            int j = 0;
            foreach (var s in propNames) {
                res[j++] = this.GetType().GetProperty(s).GetValue(this).ToString();
            }
            return res;
        }
    }

}
