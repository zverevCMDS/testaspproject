﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPtest2.Models
{
    public class Friend
    {
        public int id { get; set; }
        public string name { get; set; }
        public override string ToString()
        {
            return "Name : " + name + "\nID : " + id.ToString() + "\n";
        }
    }

}
