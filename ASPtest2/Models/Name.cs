﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASPtest2.Models
{
    public class Name
    {
        public Name(string first, string last)
        {
            this.first = first;
            this.last = last;
        }

        public string first { get; set; }
        public string last { get; set; }
        public override string ToString()
        {
            return "First Name : " + first + "\nLast Name : " + last + "\n";
        }
    }
}
