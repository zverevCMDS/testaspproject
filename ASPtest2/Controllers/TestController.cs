﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ASPtest2.Controllers
{
    public class TestController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public ContentResult GetString()
        {
            return Content("Hello");
        }

        public ContentResult Hello(string name)
        {
            
            return Content("Hello "+name +"!");
        }

        public ContentResult GetDayOfYear()
        {

            //return Content(DateTime.Now.ToString("yyyy-MM-dd"));
            return Content(DateTimeOffset.Now.ToString("D"));
        }

        public ContentResult GetTime()
        {
            
            string res = DateTime.Now.TimeOfDay.ToString();
            
            return Content(res);
        }


        public ContentResult Sum(int a, int b)
        {

            

            return Content((a+b).ToString());
        }
    }
}