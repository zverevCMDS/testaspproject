﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ASPtest2.Models;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Reflection;

namespace ASPtest2.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Hello(string name = "Unknown User")
        {
            ViewData["Name"] = name;

            return View();
        }


        public IActionResult GetData()
        {
            string json;
            using (StreamReader r = new StreamReader("sample.json"))
            {
                json  = r.ReadToEnd();
                
            }
            ViewData["People"] =  JsonConvert.DeserializeObject<List<Person>>(json);

            PropertyInfo[] properties = typeof(Person).GetProperties();
            string[] propNames = new string[properties.Length];
            for(int i = 0; i < properties.Length; i++)
            {
                propNames[i] = properties[i].Name;
            }
            ViewData["Props"] = propNames;
            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
