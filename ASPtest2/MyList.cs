﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASPtest2
{
    public class MyList<T> : List<T>
    {
        public override string ToString()
        {
            StringBuilder s = new StringBuilder();
            s.Append("(");
            foreach (var element in this)
            {
                s.Append(element.ToString() + ",");
            }
            s.Replace(",", ")", s.Length - 1, 1);
            return s.ToString();
        }
    }
}
