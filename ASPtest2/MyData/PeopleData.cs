﻿using ASPtest2.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ASPtest2.MyData
{
    public class PeopleData
    {
        public static Dictionary<string, Person> d;
        
        static PeopleData()
        {
            if (d == null)
            {
                string json;
                using (StreamReader r = new StreamReader("sample.json"))
                {
                    json = r.ReadToEnd();

                }
                var res = JsonConvert.DeserializeObject<List<Person>>(json);
                foreach(var s in res)
                {
                    d[s._id] = s;
                }
            }
        }

        public static  Person get_details(string id)
        {
            if (d[id] != null)
            {
                return d[id];
            }
            else
            {
                return new Person(name: new Name(first:"Unknown" ,last:"Person")
            }
        }
    }
}
